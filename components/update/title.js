const contentful = require('contentful-management');

const client = contentful.createClient({
	accessToken: '<content_management_token>',
});

// Update content type
client
	.getSpace('<space_id>')
	.then((space) => space.getEnvironment('<environnement_id>'))
	.then((environment) => environment.getContentType('title'))
	.then((contentType) => {
		contentType.fields = [
			{
				id: 'title',
				name: 'Title',
				type: 'Symbol',
				localized: true,
				required: true,
				validations: [],
				disabled: false,
				omitted: false,
			},
			{
				id: 'titleLevel',
				name: 'Title level',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [
					{
						in: ['h1', 'h2', 'h3', 'h4', 'h5'],
					},
				],
				defaultValue: {
					fr: 'h2',
				},
				disabled: false,
				omitted: false,
			},
			{
				id: 'themeLayout',
				name: 'Theme Layout',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [
					{
						in: ['Fullwidth', 'Boxed'],
					},
				],
				defaultValue: {
					fr: 'Fullwidth',
				},
				disabled: false,
				omitted: false,
			},
			{
				id: 'backgroundColor',
				name: 'Background Color',
				type: 'Symbol',
				localized: false,
				required: false,
				validations: [
					{
						in: [
							'Primary',
							'Link',
							'Info',
							'Success',
							'Warning',
							'Danger',
							'White',
							'Black',
							'Light',
							'Dark',
						],
					},
				],
				disabled: false,
				omitted: false,
			},
			{
				id: 'textColor',
				name: 'Text Color',
				type: 'Symbol',
				localized: false,
				required: false,
				validations: [
					{
						in: [
							'Primary',
							'Link',
							'Info',
							'Success',
							'Warning',
							'Danger',
							'White',
							'Black',
							'Light',
							'Dark',
						],
					},
				],
				disabled: false,
				omitted: false,
			},
			{
				id: 'horizontalAlignement',
				name: 'Horizontal alignement',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [
					{
						in: ['Left', 'Center', 'Right'],
					},
				],
				defaultValue: {
					fr: 'Left',
				},
				disabled: false,
				omitted: false,
			},
		];
		return contentType.update();
	})
	.then((contentType) => contentType.publish())
	.then((contentType) =>
		console.log(`Content type ${contentType.sys.id} updated and published.`)
	)
	.catch(console.error);
