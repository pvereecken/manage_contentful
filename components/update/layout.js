const contentful = require('contentful-management');

const client = contentful.createClient({
	accessToken: '<content_management_token>',
});

// Update content type
client
	.getSpace('<space_id>')
	.then((space) => space.getEnvironment('<environnement_id>'))
	.then((environment) => environment.getContentType('layout'))
	.then((contentType) => {
		contentType.fields = [
			{
				id: 'layoutTitle',
				name: 'Layout title',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [],
				disabled: false,
				omitted: false,
			},
			{
				id: 'themeLayout',
				name: 'Theme Layout',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [
					{
						in: ['Fullwidth', 'Boxed'],
					},
				],
				defaultValue: {
					fr: 'Fullwidth',
				},
				disabled: false,
				omitted: false,
			},
			{
				id: 'backgroundColor',
				name: 'Background Color',
				type: 'Symbol',
				localized: false,
				required: false,
				validations: [
					{
						in: ['White', 'Black', 'Light', 'Dark'],
					},
				],
				disabled: false,
				omitted: false,
			},
			{
				id: 'columns',
				name: 'Columns',
				type: 'Array',
				localized: false,
				required: true,
				validations: [
					{
						size: {
							min: 1,
							max: 5,
						},
					},
				],
				disabled: false,
				omitted: false,
				items: {
					type: 'Link',
					validations: [
						{
							linkContentType: ['layoutColumn'],
						},
					],
					linkType: 'Entry',
				},
			},
		];
		return contentType.update();
	})
	.then((contentType) => contentType.publish())
	.then((contentType) =>
		console.log(`Content type ${contentType.sys.id} updated and published.`)
	)
	.catch(console.error);
