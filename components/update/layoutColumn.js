const contentful = require('contentful-management');

const client = contentful.createClient({
	accessToken: '<content_management_token>',
});

// Update content type
client
	.getSpace('<space_id>')
	.then((space) => space.getEnvironment('<environnement_id>'))
	.then((environment) => environment.getContentType('layoutColumn'))
	.then((contentType) => {
		contentType.fields = [
			{
				id: 'title',
				name: 'Title',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [],
				disabled: false,
				omitted: false,
			},
			{
				id: 'backgroundColor',
				name: 'Background Color',
				type: 'Symbol',
				localized: false,
				required: false,
				validations: [
					{
						in: ['White', 'Black', 'Light', 'Dark'],
					},
				],
				disabled: false,
				omitted: false,
			},
			{
				id: 'verticalAlignement',
				name: 'Vertical alignement',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [
					{
						in: ['Top', 'Center', 'Bottom'],
					},
				],
				defaultValue: {
					fr: 'Top',
				},
				disabled: false,
				omitted: false,
			},
			{
				id: 'horizontalAlignement',
				name: 'Horizontal alignement',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [
					{
						in: ['Left', 'Center', 'Right'],
					},
				],
				defaultValue: {
					fr: 'Left',
				},
				disabled: false,
				omitted: false,
			},
			{
				id: 'columnWidth',
				name: 'Column Width',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [
					{
						in: [
							'Three quarters',
							'Two thirds',
							'half',
							'One third',
							'One quarter',
							'Auto',
							'Four fifths',
							'Three fifths',
							'Two fifths',
							'One fifth',
							'Fullwidth',
						],
					},
				],
				defaultValue: {
					fr: 'Auto',
				},
				disabled: false,
				omitted: false,
			},
			{
				id: 'customBlocks',
				name: 'Custom blocks',
				type: 'Array',
				localized: false,
				required: true,
				validations: [],
				disabled: false,
				omitted: false,
				items: {
					type: 'Link',
					validations: [
						{
							linkContentType: [
								'gallery',
								'4QWAQYgi4o6nBG5iCgeuJ3',
								'417dD43LV8jsFejR1XI60g',
								'space',
								'textContent',
								'title',
							],
						},
					],
					linkType: 'Entry',
				},
			},
		];
		return contentType.update();
	})
	.then((contentType) => contentType.publish())
	.then((contentType) =>
		console.log(`Content type ${contentType.sys.id} updated and published.`)
	)
	.catch(console.error);
