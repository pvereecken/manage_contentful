const contentful = require('contentful-management');

const client = contentful.createClient({
	accessToken: '<content_management_api_key>',
});

client
	.getSpace('<space_id>')
	.then((space) => space.getEnvironment('<environment_id>'))
	.then((environment) =>
		environment.createContentTypeWithId('heroBanner', {
			name: 'Hero banner',
			description: '',
			displayField: 'heroTitle',
			fields: [
				{
					id: 'heroTitle',
					name: 'Hero Title',
					type: 'Symbol',
					localized: true,
					required: false,
					validations: [],
					disabled: false,
					omitted: false,
				},
				{
					id: 'displayContentText',
					name: 'Display Content Text',
					type: 'Boolean',
					localized: false,
					required: true,
					validations: [],
					defaultValue: {
						fr: true,
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'textColor',
					name: 'Text Color',
					type: 'Symbol',
					localized: false,
					required: false,
					validations: [
						{
							in: [
								'Primary',
								'Link',
								'Info',
								'Success',
								'Warning',
								'Danger',
								'White',
								'Black',
								'Light',
								'Dark',
							],
						},
					],
					disabled: false,
					omitted: false,
				},
				{
					id: 'heroSubtitle',
					name: 'Hero subtitle',
					type: 'Symbol',
					localized: true,
					required: false,
					validations: [],
					disabled: false,
					omitted: false,
				},
				{
					id: 'backgroundColor',
					name: 'Background Color',
					type: 'Symbol',
					localized: false,
					required: false,
					validations: [
						{
							in: [
								'Primary',
								'Link',
								'Info',
								'Success',
								'Warning',
								'Danger',
								'White',
								'Black',
								'Light',
								'Dark',
							],
						},
					],
					disabled: false,
					omitted: false,
				},
				{
					id: 'heroSize',
					name: 'Hero size',
					type: 'Symbol',
					localized: false,
					required: true,
					validations: [
						{
							in: ['Small', 'Medium', 'Large', 'Half height', 'Full height'],
						},
					],
					defaultValue: {
						fr: 'Medium',
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'horizontalAlignement',
					name: 'Horizontal alignement',
					type: 'Symbol',
					localized: false,
					required: true,
					validations: [
						{
							in: ['Left', 'Center', 'Right'],
						},
					],
					defaultValue: {
						fr: 'Left',
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'backgroundMedia',
					name: 'Background Media',
					type: 'Link',
					localized: false,
					required: false,
					validations: [
						{
							linkMimetypeGroup: ['image', 'video'],
						},
					],
					disabled: false,
					omitted: false,
					linkType: 'Asset',
				},
				{
					id: 'parallaxBackgroundImage',
					name: 'Parallax Background Image',
					type: 'Boolean',
					localized: false,
					required: true,
					validations: [],
					defaultValue: {
						fr: false,
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'link',
					name: 'Link',
					type: 'Symbol',
					localized: true,
					required: false,
					validations: [],
					disabled: false,
					omitted: false,
				},
				{
					id: 'linkText',
					name: 'Link Text',
					type: 'Symbol',
					localized: true,
					required: false,
					validations: [],
					disabled: false,
					omitted: false,
				},
				{
					id: 'linkType',
					name: 'Link Type',
					type: 'Symbol',
					localized: false,
					required: false,
					validations: [
						{
							in: ['Text', 'Button'],
						},
					],
					disabled: false,
					omitted: false,
				},
				{
					id: 'linkColor',
					name: 'Link Color',
					type: 'Symbol',
					localized: false,
					required: false,
					validations: [
						{
							in: [
								'White',
								'Light',
								'Dark',
								'Black',
								'Primary',
								'Link',
								'Info',
								'Success',
								'Warning',
								'Danger',
							],
						},
					],
					disabled: false,
					omitted: false,
				},
				{
					id: 'linkSize',
					name: 'Link Size',
					type: 'Symbol',
					localized: false,
					required: false,
					validations: [
						{
							in: ['Small', 'Normal', 'Medium', 'Large'],
						},
					],
					disabled: false,
					omitted: false,
				},
				{
					id: 'targetBlank',
					name: 'Target blank ?',
					type: 'Boolean',
					localized: false,
					required: true,
					validations: [],
					defaultValue: {
						fr: false,
					},
					disabled: false,
					omitted: false,
				},
			],
		})
	)
	.then((contentType) => contentType.publish())
	.then((contentType) =>
		console.log(`Content type ${contentType.sys.id} created and activated.`)
	)
	.catch(console.error);
