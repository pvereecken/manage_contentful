const contentful = require('contentful-management');

const client = contentful.createClient({
	accessToken: '<content_management_api_key>',
});

client
	.getSpace('<space_id>')
	.then((space) => space.getEnvironment('<environment_id>'))
	.then((environment) =>
		environment.createContentTypeWithId('space', {
			name: 'Space',
			description: '',
			displayField: 'title',
			fields: [
				{
					id: 'title',
					name: 'Title',
					type: 'Symbol',
					localized: false,
					required: true,
					validations: [],
					disabled: false,
					omitted: false,
				},
				{
					id: 'height',
					name: 'Height',
					type: 'Symbol',
					localized: false,
					required: true,
					validations: [],
					disabled: false,
					omitted: false,
				},
				{
					id: 'units',
					name: 'Units',
					type: 'Symbol',
					localized: false,
					required: true,
					validations: [
						{
							in: ['px', 'rem'],
						},
					],
					defaultValue: {
						fr: 'px',
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'backgroundColor',
					name: 'Background Color',
					type: 'Symbol',
					localized: false,
					required: false,
					validations: [
						{
							in: [
								'Primary',
								'Link',
								'Info',
								'Success',
								'Warning',
								'Danger',
								'White',
								'Black',
								'Light',
								'Dark',
							],
						},
					],
					disabled: false,
					omitted: false,
				},
			],
		})
	)
	.then((contentType) => contentType.publish())
	.then((contentType) =>
		console.log(`Content type ${contentType.sys.id} created and activated.`)
	)
	.catch(console.error);
