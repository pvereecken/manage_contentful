const contentful = require('contentful-management');

const client = contentful.createClient({
	accessToken: '<content_management_api_key>',
});

client
	.getSpace('<space_id>')
	.then((space) => space.getEnvironment('<environment_id>'))
	.then((environment) =>
		environment.createContentTypeWithId('gallery', {
			name: 'Gallery',
			description: '',
			displayField: 'title',
			fields: [
				{
					id: 'title',
					name: 'Title',
					type: 'Symbol',
					localized: true,
					required: true,
					validations: [],
					disabled: false,
					omitted: false,
				},
				{
					id: 'titleLevel',
					name: 'Title level',
					type: 'Symbol',
					localized: false,
					required: true,
					validations: [
						{
							in: ['Do not display', 'h1', 'h2', 'h3', 'h4', 'h5'],
						},
					],
					defaultValue: {
						fr: 'Do not display',
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'textColor',
					name: 'Text Color',
					type: 'Symbol',
					localized: false,
					required: false,
					validations: [
						{
							in: [
								'Primary',
								'Link',
								'Info',
								'Success',
								'Warning',
								'Danger',
								'White',
								'Black',
								'Light',
								'Dark',
							],
						},
					],
					disabled: false,
					omitted: false,
				},
				{
					id: 'themeLayout',
					name: 'Theme Layout',
					type: 'Symbol',
					localized: false,
					required: true,
					validations: [
						{
							in: ['Fullwidth', 'Boxed'],
						},
					],
					defaultValue: {
						fr: 'Fullwidth',
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'backgroundColor',
					name: 'Background Color',
					type: 'Symbol',
					localized: false,
					required: false,
					validations: [
						{
							in: [
								'Primary',
								'Link',
								'Info',
								'Success',
								'Warning',
								'Danger',
								'White',
								'Black',
								'Light',
								'Dark',
							],
						},
					],
					disabled: false,
					omitted: false,
				},
				{
					id: 'horizontalAlignement',
					name: 'Horizontal alignement',
					type: 'Symbol',
					localized: false,
					required: true,
					validations: [
						{
							in: ['Left', 'Center', 'Right', 'Space Between', 'Space Around'],
						},
					],
					defaultValue: {
						fr: 'Left',
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'imagesPerRow',
					name: 'Media per row',
					type: 'Integer',
					localized: false,
					required: true,
					validations: [
						{
							range: {
								min: 1,
								max: 5,
							},
						},
					],
					defaultValue: {
						fr: 1,
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'coverImage',
					name: 'Cover image',
					type: 'Boolean',
					localized: false,
					required: true,
					validations: [],
					defaultValue: {
						fr: false,
					},
					disabled: false,
					omitted: false,
				},
				{
					id: 'medias',
					name: 'Medias',
					type: 'Array',
					localized: false,
					required: true,
					validations: [],
					disabled: false,
					omitted: false,
					items: {
						type: 'Link',
						validations: [
							{
								linkMimetypeGroup: ['image'],
							},
						],
						linkType: 'Asset',
					},
				},
			],
		})
	)
	.then((contentType) => contentType.publish())
	.then((contentType) =>
		console.log(`Content type ${contentType.sys.id} created and activated.`)
	)
	.catch(console.error);
