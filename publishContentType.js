const contentful = require('contentful-management');

const client = contentful.createClient({
	accessToken: '<content_management_api_key>',
});

client
	.getSpace('<space_id>')
	.then((space) => space.getEnvironment('<environment_id>'))
	.then((environment) => environment.getContentType('<content_type_id>'))
	.then((contentType) => contentType.publish())
	.then((contentType) =>
		console.log(`Content type ${contentType.sys.id} activated.`)
	)
	.catch(console.error);
