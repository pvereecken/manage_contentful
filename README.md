# Comment utiliser l'API Contentful Management

La documentation complète est disponible ici : [https://www.contentful.com/developers/docs/references/content-management-api/](https://www.contentful.com/developers/docs/references/content-management-api/)

# Fichiers

4 fichiers exemples sont disponibles :

- La création d'un nouveau ContentType
- L'update d'un ContentType
- La publication d'un ContentType
- La désactivation d'un ContentType

## Création d'un nouveau ContentType

**Fichier : newContentType.js**
3 variables sont à changer :

- <content_management_token>
- <space_id>
- <environnement_id> (si pas d'environnement : 'master')

Le contentType donné dans ce fichier est donné à titre indicatif. Pour en savoir plus : [https://www.contentful.com/developers/docs/references/content-management-api/#/reference/content-types/content-type](https://www.contentful.com/developers/docs/references/content-management-api/#/reference/content-types/content-type)

### Dupliquer un élément du design systeme

Tous les éléments du design systeme peuvent être dupliquer dans votre projet Contentful. Pour vous simplifier la tâche, chacun d'eux possède son propre fichier js de création. Ils se trouvent dans le dossier "_components_"

**Chaque ContentType créé avec cette requête sera automatiquement publié.**

**[!] Remarque :** Ne pas oublier de changer les variables spécifiées plus haut.

## Update d'un ContentType

**Fichier : updateContentType.js**
3 variables sont à changer :

- <content_management_token>
- <space_id>
- <environnement_id> (si pas d'environnement : 'master')

L'exemple donné dans ce fichier est donné à titre indicatif. Pour en savoir plus : [https://www.contentful.com/developers/docs/references/content-management-api/#/reference/content-types/content-type](https://www.contentful.com/developers/docs/references/content-management-api/#/reference/content-types/content-type)

**[!] Remarque :** L'id du contentType n'est pas son nom mais bel et bien l'id indiqué dans la colonne de droite lors de son édition dans Contentful
![enter image description here](https://i.postimg.cc/66v47zdz/content-type-id.png)

## Publication d'un ContentType

**Fichier : publishContentType.js**
4 variables sont à changer :

- <content_management_token>
- <space_id>
- <environnement_id> (si pas d'environnement : 'master')
- <content_type_id>

Par défaut, tous les contentType mis à jour ou créé via l'API sont en mode "draft". Vous pouvez les publier en passant par Contentful ou vous pouvez le faire en utilisant le contenu du fichier.

Remarque : La requête peut directement être ajouter à la suite de la création du contentType (voir fichier _examples/createAndPublish.js_)

**[!] Remarque bis :** L'id du contentType n'est pas son nom mais bel et bien l'id indiqué dans la colonne de droite lors de son édition dans Contentful
![enter image description here](https://i.postimg.cc/66v47zdz/content-type-id.png)

## Désactivation d'un ContentType

**Fichier : unpublishContentType.js**
4 variables sont à changer :

- <content_management_token>
- <space_id>
- <environnement_id> (si pas d'environnement : 'master')
- <content_type_id>

C'est action est impossible directement dans ContentFul. Un contentModel désactivé ne pourra pas être utilisé pour créer de nouveau contenu.

**[!] Remarque bis :** L'id du contentType n'est pas son nom mais bel et bien l'id indiqué dans la colonne de droite lors de son édition dans Contentful
![enter image description here](https://i.postimg.cc/66v47zdz/content-type-id.png)

# Créer un token pour l'API

Tout se passe dans Contentful. Il faut aller dans :

> Settings > API Key > Content managements tokens

Puis cliquer sur "generate personnal token". Ce token est lié à votre compte : quelque soit votre projet Contentful, il ne changera pas.

**Remarque :** si vous utilisez l'API via une app par exemple, il faudra utiliser un OAuth token (voir la [documentation](https://www.contentful.com/developers/docs/extensibility/oauth/)

**[!] Remarque :** Pensez à bien copier/coller le token avant de valider : il est impossible de le récupérer par la suite.

# Exécuter une requête

> node nomFichier.js
