const contentful = require('contentful-management');

const client = contentful.createClient({
	accessToken: '<content_management_token>',
});

// Update content type
client
	.getSpace('<space_id>')
	.then((space) => space.getEnvironment('<environnement_id>'))
	.then((environment) => environment.getContentType('<contentType_id>'))
	.then((contentType) => {
		// This is an example
		contentType.fields = [
			{
				id: 'heroTitle',
				name: 'Hero Title',
				type: 'Symbol',
				localized: true,
				required: true,
				validations: [],
				disabled: false,
				omitted: false,
			},
			{
				id: 'heroSubtitle',
				name: 'Hero subtitle',
				type: 'Symbol',
				localized: true,
				required: false,
				validations: [],
				disabled: false,
				omitted: false,
			},
			{
				id: 'backgroundColor',
				name: 'Background Color',
				type: 'Symbol',
				localized: false,
				required: false,
				validations: [
					{
						in: [
							'Primary - Modif',
							'Link',
							'Info',
							'Success',
							'Warning',
							'Danger',
						],
					},
				],
				disabled: false,
				omitted: false,
			},
			{
				id: 'heroSize',
				name: 'Hero size',
				type: 'Symbol',
				localized: false,
				required: true,
				validations: [
					{
						in: ['Small', 'Medium', 'Large', 'Half height', 'Full height'],
					},
				],
				defaultValue: {
					fr: 'Medium',
				},
				disabled: false,
				omitted: false,
			},
			{
				id: 'backgroundMedia',
				name: 'Background Media',
				type: 'Link',
				localized: false,
				required: false,
				validations: [
					{
						linkMimetypeGroup: ['image', 'video'],
					},
				],
				disabled: false,
				omitted: false,
				linkType: 'Asset',
			},
		];
		return contentType.update();
	})
	.then((contentType) =>
		console.log(`Content type ${contentType.sys.id} updated.`)
	)
	.catch(console.error);
